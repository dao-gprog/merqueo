const mongoose = require("mongoose");

const {
  MONGO_USERNAME,
  MONGO_PASSWORD,
  MONGO_HOSTNAME,
  MONGO_PORT,
  MONGO_DB
} = process.env;

const url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`;

const options = {
  poolSize: 10,
  useNewUrlParser: true,
  useCreateIndex: true,
  // Handle Auto Reconnection In MongoDB
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500,
  connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
  socketTimeoutMS: 45000,
}

let mongoDB = mongoose.createConnection(url, options, (err, res) => {
    if (err) throw err;
    console.log("\x1b[32mMONGO\x1b[0m", "Database \x1b[32m ONLINE \x1b[0m");
  }
)

module.exports = { mongoDB };