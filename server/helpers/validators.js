
/**
 * @author DanielOlaya
 * @description Validates the money's p
 * @param   {Array}    money money, it must be an array in the form [{denomination, quantity}] 
 */

module.exports.moneyDenominationValidation = ({ money }) => {
    if (!money) return { status: 400, message: `money required, it must be an array`}
    else if (!Array.isArray(money) || money.length === 0) return { status: 400, message: `money required, it must contain denomination and quantity`}
    else {
        for (let cash of money) {
            if (!cash.denomination) return { status: 400, message: `denomination is required`}
            else if (!cash.quantity) return { status: 400, message: `quantity is required`}
            else if (!Number.isInteger(cash.denomination)) return { status: 400, message: `denomination must be an integer value`}
            else if (!Number.isInteger(cash.quantity)) return { status: 400, message: `quantity must be an integer value`}
            else if (!process.env.VALID_DENOMINATIONS.includes(cash.denomination)) return { status: 400, message: `invalid denomination ${cash.denomination}`}
        }
    }
    return { status: 200, message: `ok` };
}