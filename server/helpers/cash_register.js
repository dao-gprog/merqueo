module.exports.calculateTotalAmount = (money) => {
    let product = [];
    let total_amount = 0;
    try {
        for (let cash of money) {
            product.push(cash.denomination * cash.quantity);
        }
        total_amount = product.reduce((a, b) => a + b);
    } catch (error) {
        console.log(error) ;
    }
    return total_amount;
}

module.exports.setQuantityToZero = (money) => {
    let formatedArr = []
    if (money && money.length > 0) {
      for (let cash of money) {
        formatedArr.push({ denomination: cash.denomination, quantity: 0 })
      }
    }
    return formatedArr;
}

module.exports.formatMoneyQuantity = (money, denomination, quantity) => {
    let formatedArr = []
    if (money && money.length > 0) {
      for (let cash of money) {
        formatedArr.push(cash.denomination === denomination ? { denomination, quantity: cash.quantity + quantity } : cash)
      }
    }
    return formatedArr;
}