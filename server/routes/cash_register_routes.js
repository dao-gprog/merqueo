const express = require('express');
const router = express.Router();

let cashRegister = require('./../controllers/cash_register_controller');

router.post('/load-base-money', cashRegister.loadBase);
router.get('/status', cashRegister.getCashRegisterStatus);
router.get('/empty', cashRegister.emptyCashRegister);

module.exports = router;