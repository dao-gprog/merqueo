const express = require('express');
const app = express();
const router = express.Router();
const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

//Start API Welcome mesage
app.get('/', (req, res) => {
    res.status(200).send(
        "TYBA TEST API"
    );
})

// Login Routes
app.use('/api/cash-register', require('./cash_register_routes'));

module.exports = app;