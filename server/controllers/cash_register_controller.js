const { createCashRegister, getCashRegister, emptyCashRegister } = require("../integrations/cash_register");
const { moneyDenominationValidation } = require("../helpers/validators");
const { calculateTotalAmount } = require("../helpers/cash_register");

let CashRegisterController = {}

CashRegisterController.loadBase = async(req, res) => {
    try {
        let { status, message } = moneyDenominationValidation(req.body);
        if (status !== 200) return res.status(status).json({ status, message });
        req.body.total_amount = calculateTotalAmount(req.body.money);
        let cashRegisterCreated = await createCashRegister(req.body);
        if (!cashRegisterCreated) {
            status = 400
            message = "error loading base money to the cash register"
            return res.status(status).json({ status, message });
        }
        return res.status(201).json({
            status: 201,
            message: "cash register created"
        });
    } catch (error) {
        return res.status(500).json({
            status: 500,
            message: "something went wrong",
            error
        });
    }
}

CashRegisterController.getCashRegisterStatus = async(req, res) => {
    try {
        let cashRegister = await getCashRegister();
        if (!cashRegister)
            return res.status(404).json({
                status: 404,
                cashRegister,
                message: "cash register not found",
            });
        return res.status(200).json({
            status: 200,
            cashRegister,
            message: "success request",
        });
    } catch (error) {
        return res.status(500).json({
            status: 500,
            message: "something went wrong",
            error
        });
    }
}

CashRegisterController.emptyCashRegister = async(req, res) => {
    try {
        let cashRegister = await emptyCashRegister();
        return res.status(200).json({
            status: 200,
            cashRegister,
            message: "success request",
        });
    } catch (error) {
        return res.status(500).json({
            status: 500,
            message: "something went wrong",
            error
        });
    }
}

module.exports = CashRegisterController