const mongoose = require("mongoose");
let { mongoDB } = require("../database/database");

const { Schema } = mongoose;

const Cash_register = new Schema({
  total_amount: {
    type: Number,
    description: "Cash_register' amount"
  },
  money: [{
    denomination: {
      type: Number,
    },
    quantity: {
      type: Number,
    },
    _id: false
  }]
});

Cash_register.set("collection", "Cash_register");
module.exports = mongoDB.model("Cash_register", Cash_register);