const mongoose = require("mongoose");
let { mongoDB } = require("../database/database");

const { Schema } = mongoose;

let Transactions = new Schema({
  transaction_amount: {
    type: Number,
    description: "transaction's amount"
  },
  money: [{
    denomination: {
      type: Number,
    },
    quantity: {
      type: Number,
    },
    _id: false
  }],
  income: {
    type: Boolean,
    default: false,
    description: "defines if the transaction is a income (payment or load base money => true) or an outcome (change or empty => false)"
  },
  date: {
    type: Date,
    default: Date.now()
  }
});

Transactions.set("collection", "Transactions");
module.exports = mongoDB.model("Transactions", Transactions);