let Cash_register = require("../models/cash_register");
const { setQuantityToZero } = require("../helpers/cash_register")

module.exports.createCashRegister = (cashRegisterData) => {
    return new Promise((resolve, reject) => {
        let cashRegister = new Cash_register(cashRegisterData);
        cashRegister.save().catch((err) => {
            console.error(err);
            resolve(null)
        });
        resolve(true)
    })
}

module.exports.getCashRegister = async() => {
    return new Promise(async (resolve, reject) => {
        const cashRegister = await Cash_register.find().catch((err) => {
            console.error(`error getting cash register in mongodb`, err);
            resolve(null)
        });
        if (!cashRegister || !cashRegister[0]) return(null);
        resolve(cashRegister[0]);
        // return formatCashRegister(cashRegister[0]);
    })
}

module.exports.updateCashRegister = (cashRegisterData) => {
    return new Promise(async(resolve, reject) => {
        let cashRegister = await Cash_register.find().catch((err) => {
            console.error(`error getting cash register in mongodb`);
            resolve(null)
        });
        if (!cashRegister || !cashRegister[0]) resolve(null);
        cashRegister[0] = cashRegisterData;
        cashRegister[0].save(() => {
          resolve(true)
        })
    })
}

module.exports.emptyCashRegister = () => {
    return new Promise(async(resolve, reject) => {
        let cashRegister = await Cash_register.find().catch((err) => {
            console.error(`error getting cash register in mongodb`);
            resolve(null)
        });
        if (!cashRegister || !cashRegister[0]) resolve(null);
        cashRegister[0].money = setQuantityToZero(cashRegister[0].money);
        cashRegister[0].total_amount = 0;
        cashRegister[0].save(() => {
          resolve(true)
        })
    })
}

// const setQuantityToZero = (money) => {
//     let formatedArr = []
//     if (money && money.length > 0) {
//       for (let cash of money) {
//         formatedArr.push({ denomination: cash.denomination, quantity: 0 })
//       }
//     }
//     return formatedArr;
// }

// const formatMoneyQuantity = (money, denomination, quantity) => {
//     let formatedArr = []
//     if (money && money.length > 0) {
//       for (let cash of money) {
//         formatedArr.push(cash.denomination === denomination ? { denomination, quantity: cash.quantity + quantity } : cash)
//       }
//     }
//     return formatedArr;
// }
