# Tyba Backend Ingeneer Tets

----
#### Autor: Daniel Olaya [@DanielOlaya]

##  Descripción General
Esta es una API REST para básica para registro y login de usuarios, consulta de restaurantes más cercanos de acuerdo a su ubicación y consulta de las transacciones hechas.

Este proyecto fue realizado en NodeJS con el framework Express usando Docker Compose.

##  Para correr el proyecto

Clonar el repositorio:
```
$ git clone > https://github.com/DanielOlaya/tyba_test.git
```

Entrar a la carpeta del proyecto:
```
$ cd tyba_test
```

Correr el proyecto con el siguiente comando:
```
$ docker-compose -f compose.yml up --build 
```

## Endpoints

### Crear usuario:
####Endpont:
> http://localhost:3000/api/users/create

####Método:
POST

####Body:
> {
    "name": "user name",
    "email": "user@gmail.com",
    "password": "userpw"
}

### Login usuario:
####Endpont:
> http://localhost:3000/api/login

####Método:
POST

####Body:
> {
    "email": "user@gmail.com",
    "password": "userpw",
    "lat": "4.76",
    "lng": "-74.78"
}

### Traer restaurantes cercanos a las coordenadas del usuario:
####Endpont:
> http://localhost:3000/api/user/get_near_restaurants/:id_user

####Método:
GET

####Params:
> id_user: 448b2230-b5d2-11ea-b1b9-d5dfd52c06d0

### Crear transacciones al usuario:
####Endpont:
> http://localhost:3000/api/user/transaction/create

####Método:
POST

####Body:
> {
    "id_user": "448b2230-b5d2-11ea-b1b9-d5dfd52c06d0",
    "restaurant": "restaurant name",
    "amount": "9500"
}

### Traer las transacciones realizadas por el usuario:
####Endpont:
> http://localhost:3000/api/user/transactions/:id_user

####Método:
GET

####Params:
> id_user: 448b2230-b5d2-11ea-b1b9-d5dfd52c06d0

### Logout usuario:
####Endpont:
> http://localhost:3000/api/logout/:id_user

####Método:
GET

####Params:
> id_user: 448b2230-b5d2-11ea-b1b9-d5dfd52c06d0

## Información

Para más información contactarme al correo da.olaya@hotmail.com

