const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const colors = require('colors');
const bodyParser = require('body-parser');
const { mongoDB } = require('./server/database/database');

const app = express();

app.use(express.json());
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }));
/** Implement Morgan **/
app.use(morgan('dev'));

/** Routes **/
app.use(require('./server/routes/routes'));

/*** Listen the port to use express ***/
app.listen(process.env.PORT, (err, res) => {
    if (err) throw err;
    console.log(`SERVER ` + colors.green('RUNNING ') + `ON PORT ` + colors.blue(process.env.PORT));
});